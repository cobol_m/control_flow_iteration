       IDENTIFICATION DIVISION. 
       PROGRAM-ID. STAR10V1.
       AUTHOR. MUKKU.
       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  COUNT-S    PIC   99    VALUE ZERO. 
       PROCEDURE DIVISION.
       000-BEGIN.
      *    DISPLAY "**********"
           PERFORM 001-PRINT-STAR-INLINE THRU 001-EXIT  
           GOBACK 
           .
       001-PRINT-STAR-INLINE.
           PERFORM 002-PRINT-ONE-STAR WITH TEST BEFORE 
           UNTIL COUNT-S = 10
           .
       001-EXIT.
           EXIT.
       002-PRINT-ONE-STAR.
           DISPLAY "*" WITH NO ADVANCING 
           COMPUTE COUNT-S = COUNT-S + 1
           .